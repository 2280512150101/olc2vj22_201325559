// Generated from D:/Usuario/lmencos/Documentos/Github/Proyecto/Proyecto 1/Proyecto1_201325559/src\Gramatica.g4 by ANTLR 4.10.1
package Gramatica;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GramaticaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GramaticaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(GramaticaParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#lrutinas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLrutinas(GramaticaParser.LrutinasContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#rutinas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRutinas(GramaticaParser.RutinasContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#funcion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncion(GramaticaParser.FuncionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#subrutina}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubrutina(GramaticaParser.SubrutinaContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#ldeclP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLdeclP(GramaticaParser.LdeclPContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#declParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclParams(GramaticaParser.DeclParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#linstrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLinstrucciones(GramaticaParser.LinstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstrucciones(GramaticaParser.InstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion(GramaticaParser.DeclaracionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo(GramaticaParser.TipoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#lid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLid(GramaticaParser.LidContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asigna}
	 * labeled alternative in {@link GramaticaParser#asig}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsigna(GramaticaParser.AsignaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code decla}
	 * labeled alternative in {@link GramaticaParser#asig}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecla(GramaticaParser.DeclaContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacion(GramaticaParser.AsignacionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#arreglo_dec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglo_dec(GramaticaParser.Arreglo_decContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#arreglo_din_dec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglo_din_dec(GramaticaParser.Arreglo_din_decContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#arreglo_asig_all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglo_asig_all(GramaticaParser.Arreglo_asig_allContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#arreglo_asig_tam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglo_asig_tam(GramaticaParser.Arreglo_asig_tamContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(GramaticaParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#llamada}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLlamada(GramaticaParser.LlamadaContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#imprimir}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImprimir(GramaticaParser.ImprimirContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#lexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLexpr(GramaticaParser.LexprContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#cond_if_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_if_else(GramaticaParser.Cond_if_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#cond_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_if(GramaticaParser.Cond_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#loop_do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_do(GramaticaParser.Loop_doContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#loop_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_while(GramaticaParser.Loop_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#loop_exit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_exit(GramaticaParser.Loop_exitContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#loop_salto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_salto(GramaticaParser.Loop_saltoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment(GramaticaParser.CommentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrExpr(GramaticaParser.StrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(GramaticaParser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntExpr(GramaticaParser.IntExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpr(GramaticaParser.OpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code llamExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLlamExpr(GramaticaParser.LlamExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code accArr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccArr(GramaticaParser.AccArrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code decExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecExpr(GramaticaParser.DecExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpr(GramaticaParser.BoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(GramaticaParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdExpr(GramaticaParser.IdExprContext ctx);
}