package Tipos;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Arreglo {

    public String id;
    public String tipo;
    public int dimension;
    public ArrayList<Object> valores = new ArrayList<Object>();

    public Arreglo(String id, String tipo, int dimension, ArrayList<Object> valores) {
        this.dimension = dimension;
        this.tipo = tipo;
        this.id = id;
        this.valores.addAll(valores);
    }
}
