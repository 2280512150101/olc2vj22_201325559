package Tipos;

import java.util.ArrayList;
import Entorno.Entorno;
import Entorno.Simbolo;
import Gramatica.GramaticaParser;
public class Funcion {
    public String nombre;
    public ArrayList<Simbolo> lparametros;
    public Object linstrucciones;
    public Entorno ent;
    public GramaticaParser.LdeclPContext ldeclaracionParam;
    public String resul;

    public Object valor;

    public Funcion(String nombre, ArrayList<Simbolo> lparametros, Object linstrucciones, GramaticaParser.LdeclPContext ldeclaracionParam, String resul) {
        this.nombre = nombre;
        this.lparametros = lparametros;
        this.linstrucciones = linstrucciones;
        this.ldeclaracionParam = ldeclaracionParam;
        this.resul = resul;
    }
}
