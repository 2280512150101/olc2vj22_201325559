package Tipos;

public class Declaracion {

    public String ID;
    public Object valor;

    public String tipo;
    public int linea;
    public int columna;

    public Declaracion(String ID, Object valor, String tipo, int linea, int columna) {
        this.ID = ID;
        this.valor = valor;
        this.tipo = tipo;
        this.linea = linea;
        this.columna = columna;
    }
}
