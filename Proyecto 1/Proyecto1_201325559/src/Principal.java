import javax.swing.*;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import javax.swing.JButton;
import javax.swing.JFrame;

import Entorno.Entorno;
import Entorno.ErrorAnalizador;
import Gramatica.*;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class Principal implements ActionListener {
    JButton btnInterpretar, btnTraducir, btnReportes, btnLimpiar;//creando variables globales de los botones
    JScrollPane scrollpane1, scrollpane2, scrollpane3, scrollpane4;
    JTextArea jta1, jta2, jta3;
    JLabel msj, esp;
    JFrame jf = new JFrame("Proyecto 1");//creacion de ventana con el titulo
    JFrame frame = new JFrame("Antlr AST");
    JPanel panel = new JPanel();
    ArrayList<ErrorAnalizador> errores = new ArrayList<ErrorAnalizador>();

    public Principal() {//constructor de la clase

        jf.setLayout(new FlowLayout());//Configurar como se dispondra el espacio del jframe
        Dimension d = new Dimension();//objeto para obtener el ancho de la pantalla

        //Areas de texto
        jta1     = new JTextArea(25,30);
        jta2     = new JTextArea(25,30);
        jta3     = new JTextArea(10,60);
        msj      = new JLabel();
        esp     = new JLabel();

        jta1.setMargin(new Insets(10,10,10,5));;
        jta2.setMargin(new Insets(10,10,10,5));;
        jta3.setMargin(new Insets(10,10,10,5));;

        scrollpane1 = new JScrollPane(jta1);
        scrollpane2 = new JScrollPane(jta2);
        scrollpane3 = new JScrollPane(jta3);
        scrollpane4 = new JScrollPane(panel);


        //Instanciando boton con texto
        btnInterpretar = new JButton("Interpretar");
        btnTraducir = new JButton("Traducir");
        btnReportes = new JButton("Reportes");
        btnLimpiar = new JButton("Nuevo");

        //añadiendo objetos a la ventana
        jf.add(scrollpane1);
        jf.add(scrollpane2);
        jf.add(scrollpane3);
        jf.add(msj);
        jf.add(btnInterpretar);
        jf.add(btnTraducir);
        jf.add(btnReportes);
        jf.add(btnLimpiar);
        jf.add(esp);


        btnInterpretar.addActionListener(this);
        btnTraducir.addActionListener(this);
        btnReportes.addActionListener(this);
        btnLimpiar.addActionListener(this);

        btnInterpretar.setMinimumSize(new Dimension(100,30));
        btnInterpretar.setMaximumSize(new Dimension(100,30));
        btnInterpretar.setPreferredSize(new Dimension(100,30));

        btnTraducir.setMinimumSize(new Dimension(100,30));
        btnTraducir.setMaximumSize(new Dimension(100,30));
        btnTraducir.setPreferredSize(new Dimension(100,30));

        btnReportes.setMinimumSize(new Dimension(100,30));
        btnReportes.setMaximumSize(new Dimension(100,30));
        btnReportes.setPreferredSize(new Dimension(100,30));

        btnLimpiar.setMinimumSize(new Dimension(100,30));
        btnLimpiar.setMaximumSize(new Dimension(100,30));
        btnLimpiar.setPreferredSize(new Dimension(100,30));

        msj.setMinimumSize(new Dimension(120,30));
        msj.setMaximumSize(new Dimension(120,30));
        msj.setPreferredSize(new Dimension(120,30));

        esp.setMinimumSize(new Dimension(120,30));
        esp.setMaximumSize(new Dimension(120,30));
        esp.setPreferredSize(new Dimension(120,30));

        //margenes para texto en boton
        btnInterpretar.setMargin(new Insets(1, 5, 1, 5));
        btnTraducir.setMargin(new Insets(1, 5, 1, 5));
        btnReportes.setMargin(new Insets(1, 5, 1, 5));
        btnLimpiar.setMargin(new Insets(1, 5, 1, 5));

        //añadiendo el listener a los botones para manipular los eventos del click
        btnInterpretar.addActionListener(this);

        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//finaliza el programa cuando se da click en la X
        jf.setResizable(false);//para configurar si se redimensiona la ventana
        jf.setLocation((int) ((d.getWidth() / 2) + 290), 50);//para ubicar inicialmente donde se muestra la ventana (x, y)
        jf.setSize(740, 700);//configurando tamaño de la ventana (ancho, alto)
        jf.setVisible(true);//configurando visualización de la venta
    }

    @Override
    public void actionPerformed(ActionEvent e) {//sobreescribimos el metodo del listener

        if(e.getSource()==btnInterpretar){//podemos comparar por el contenido del boton
            String input, area2, consola;
            msj.setText("");
            errores.clear();
            errores.add(new ErrorAnalizador(-101,-101, "Descripcion", null));
            //int var1 = 54+17*78;
            input = jta1.getText();
            area2 = jta2.getText();
            consola = jta3.getText();

            CharStream cs = fromString(input);
            GramaticaLexer lexico = new GramaticaLexer(cs);

            lexico.removeErrorListeners();
            lexico.addErrorListener(new BaseErrorListener(){
                @Override
                public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionLine, String msg, RecognitionException e)
                {
                    errores.add(new ErrorAnalizador(line, charPositionLine, msg, ErrorAnalizador.ErrorTipo.Lexico));
                }
            });

            CommonTokenStream tokens = new CommonTokenStream(lexico);
            GramaticaParser sintactico = new GramaticaParser(tokens);
            sintactico.removeErrorListeners();
            sintactico.addErrorListener(new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
                {
                    errores.add(new ErrorAnalizador(line, charPositionInLine, msg, ErrorAnalizador.ErrorTipo.Sintactico));
                }
            });
            GramaticaParser.StartContext startCtx = sintactico.start();
            Visitor visitor = new Visitor(new Entorno(null), new Stack<Entorno>(), false);
            visitor.visit(startCtx);

            errores.addAll(visitor.errores);
            jta3.setText(visitor.getPrint());
            visitor.graficarTabla();
            //JOptionPane.showMessageDialog(null, "Análisis completo", "Completo", JOptionPane.INFORMATION_MESSAGE);
            msj.setText("Analisis completo");

/*
            //SEGUNDA PASADA, PARA 3D
            Visitor visitor3d = new Visitor(visitor.padre, visitor.pilaEnt, true);
            visitor3d.visit(startCtx);
            String todo3d = "";
            todo3d = visitor3d.c3d.getHeader() + "\n";
            for(String s: visitor3d.c3d.codigo)
                todo3d += s + '\n';

            jta2.setText(todo3d);
*/
            //GRAFICA DE AST

            //ParseTree tree = sintactico.start();
            //System.out.println(tree.toStringTree(sintactico));
            //System.out.println(startCtx.toStringTree());
            TreeViewer viewr = new TreeViewer(Arrays.asList(sintactico.getRuleNames()),startCtx);
            viewr.setScale(2.0);
            panel.add(viewr);

            /*
            ParseTree tree = sintactico.start();
            List<String> rulesName = Arrays.asList(sintactico.getRuleNames());
            TreeViewer viewr = new TreeViewer(rulesName,startCtx);
            viewr.open();
             */

            frame.add(scrollpane4);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setSize(200,200);
        }

        else if(e.getSource()==btnReportes){
            String reportError = "digraph E { node [shape=plaintext]; tabla [label=<<TABLE>";
            for (ErrorAnalizador err : errores)
                reportError += err.toString();
            reportError += "</TABLE>>]; }";

            FileWriter file = null;
            try {
                file = new FileWriter("errores.dot");
                file.write(reportError);
                file.close();
                Runtime.getRuntime().exec("dot -Tpdf errores.dot -o errores.pdf");
                //JOptionPane.showMessageDialog(null, "Reportes generados.", "Completo", JOptionPane.INFORMATION_MESSAGE);
                msj.setText("Reportes generados.");

            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }

            frame.setVisible(true);
        }

        else if(e.getSource()==btnTraducir){

            String input = jta1.getText();
            CharStream cs = fromString(input);
            //Primera pasada
            GramaticaLexer lexico = new GramaticaLexer(cs);

            lexico.removeErrorListeners();
            lexico.addErrorListener(new BaseErrorListener(){
                @Override
                public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionLine, String msg, RecognitionException e)
                {
                    errores.add(new ErrorAnalizador(line, charPositionLine, msg, ErrorAnalizador.ErrorTipo.Lexico));
                }
            });

            CommonTokenStream tokens = new CommonTokenStream(lexico);
            GramaticaParser sintactico = new GramaticaParser(tokens);
            sintactico.removeErrorListeners();
            sintactico.addErrorListener(new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
                {
                    errores.add(new ErrorAnalizador(line, charPositionInLine, msg, ErrorAnalizador.ErrorTipo.Sintactico));
                }
            });
            GramaticaParser.StartContext startCtx = sintactico.start();
            Visitor visitor = new Visitor(new Entorno(null), new Stack<Entorno>(), false);
            visitor.visit(startCtx);


            //SEGUNDA PASADA, PARA 3D
            Visitor visitor3d = new Visitor(visitor.padre, visitor.pilaEnt, true);
            visitor3d.visit(startCtx);
            String todo3d = "";
            todo3d = visitor3d.c3d.getHeader() + "\n";
            for(String s: visitor3d.c3d.codigo)
                todo3d += s + '\n';

            jta2.setText(todo3d);
        }

        else if(e.getSource()==btnLimpiar){
            jta2.setText("");
            jta3.setText("");
            msj.setText("");
        }

    }
}
