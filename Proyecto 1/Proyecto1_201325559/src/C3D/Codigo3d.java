package C3D;

import Entorno.Simbolo;

import java.util.ArrayList;

public class Codigo3d {

    public ArrayList<String> codigo;
    private int temp;
    private int etiq;

    public Codigo3d() {
        this.codigo = new ArrayList<>();
        this.temp = -1;
        this.etiq = -1;
    }

    public String generarTemp(){
        this.temp = this.temp + 1;
        return String.valueOf("t" + this.temp);
    }

    public String ultimoTemp(){
        return String.valueOf("t" + this.temp);
    }

    public String generarEtiq(){
        this.etiq = this.etiq + 1;
        return String.valueOf("L" + this.etiq);
    }

    public Simbolo obtenerAnd(String izq, String der)
    {
        this.codigo.add("if (" + izq + " ) goto " + this.generarEtiq() + ";\n" +
                "goto " + this.generarEtiq() + ";\n" +
                "L" + (this.etiq - 1) + ": if (" + der + ") goto L" + (this.etiq + 1) + ";\n" +
                "goto L" + (this.etiq + 2) + ";\n" +
                this.generarEtiq() + ": " + this.generarTemp() + " = 1;\n" +
                "goto L" + (this.etiq + 2) + ";\n" +
                "L" + (this.etiq - 1) + ": " + this.generarEtiq() + ": t" + this.temp + "= 0;\n" +
                this.generarEtiq() + ":\n");
        return  new Simbolo(Simbolo.TipoSimbolo.C3D, this.ultimoTemp(), "FLOAT");
    }

    private String imprimir_variable()
    {
        String tempIni = this.generarTemp();
        String etiqIni = this.generarEtiq();
        return "void imprimir_variable()\n" +
                "{\n" +
                tempIni + " = stack[(int)p];\n" +
                etiqIni + ":\n" +
                this.generarTemp() + " = heap[(int)" + tempIni + "];\n" +
                "if (" + this.ultimoTemp() + " != -1) goto L" + (this.etiq + 1) + ";\n" +
                "goto L" + (this.etiq + 2) + ";\n" +
                this.generarEtiq() + ":\n" +
                "printf(\"%c\", (char)" + this.ultimoTemp() + ");\n" +
                tempIni + "=" + tempIni + " + 1;\n" +
                "goto " + etiqIni + ";\n" +
                this.generarEtiq() + ":\n" +
                "printf(\"%c\\n\", (char)32);\n" +
                "return;\n" +
                "}\n\n";
    }

    private String imprimir_string()
    {
        String tempStart = this.generarTemp();
        String labelStart = this.generarEtiq();
        return "void imprimir_string()\n" +
                "{\n" +
                tempStart + " = p;\n" +
                labelStart + ":\n" +
                this.generarTemp() + " = heap[(int)" + tempStart + "];\n" +
                "if (" + this.ultimoTemp() + " != -1) goto L" + (this.etiq + 1) + ";\n" +
                "goto L" + (this.etiq + 2) + ";\n" +
                this.generarEtiq() + ":\n" +
                "printf(\"%c\", (char)" + this.ultimoTemp() + ");\n" +
                tempStart + "=" + tempStart + " + 1;\n" +
                "goto " + labelStart + ";\n" +
                this.generarEtiq() + ":\n" +
                "printf(\"%c\\n\", (char)32);\n" +
                "return;\n" +
                "}\n\n";
    }

    private String imprimir_varInt(){
        String imprimible = "";
        imprimible = "void imprimir_var_Entero()\n{\n" +
                    this.generarTemp() + " = stack[(int)p];\n" +
                    "printf(\"%f\\n\", " + this.ultimoTemp() + ");" +
                    "\nreturn;\n}\n\n";
        return imprimible;
    }

    public String getHeader(){
        String imprimible = this.imprimir_variable() + this.imprimir_string() +  this.imprimir_varInt();
        String listTemp = "";
        for(int i = 0; i <= this.temp; i++)
            listTemp += "t" + String.valueOf(i) + (i < this.temp ? "," : ";\n");
        String header;
        header = "#include <stdio.h>\n" +
                "double stack[30101999];\n" +
                "double heap[30101999];\n" +
                "double p;\n" +
                "double h;\n" +
                "double " + listTemp + "\n" +
                imprimible;

        return header;
    }



}
