import C3D.Codigo3d;
import Gramatica.*;
import Entorno.*;
import Entorno.Simbolo.*;
import Tipos.Arreglo;
import Tipos.Declaracion;
import Tipos.Funcion;
import Tipos.Subrutina;
import org.antlr.v4.codegen.model.decl.Decl;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Visitor extends GramaticaBaseVisitor<Object> {

    Stack<Entorno> pilaEnt;
    Entorno padre;
    ArrayList<ErrorAnalizador> errores = new ArrayList<ErrorAnalizador>();
    protected String printable = "";
    boolean es3d;
    Codigo3d c3d = new Codigo3d();

    public Visitor(Entorno ent, Stack<Entorno> pilaEnt, boolean es3d){
        this.pilaEnt = pilaEnt;
        this.pilaEnt.push(ent);
        this.padre = ent;
        this.es3d = es3d;
    }

    public void setPrint(String texto){
        printable = printable + texto;
    }

    public String getPrint(){
        return printable;
    }

    public void graficarTabla(){

        Entorno ent = pilaEnt.peek();

        String reportTabla = "digraph E { graph [ratio=fill]; node [shape=plaintext] arset [label=<<TABLE>" +
                "<tr><td> Tipo </td>" +
                "<td> Valor </td>" +
                "<td> ID </td>" +
                "<td> TipoSim </td></tr>";

        for(Simbolo s : ent.getTablaSimbolo().values()){
            reportTabla += "<tr><td>" + s.tipo.toString()  + "</td>";
                        if(s.tipoSimbolo.name().toString().equals("Funcion")){
                            Funcion f = (Funcion)s.valor;
                            reportTabla += "<td>" + f.valor + "</td>";
                        }else if (s.tipoSimbolo.name().toString().equals("Subrutina")){
                            reportTabla += "<td>" + "" + "</td>";
                        } else{
                            reportTabla += "<td>" + s.valor + "</td>";
                        }
            reportTabla +="<td>" + s.identificador.toString()  + "</td>" +
                        "<td>" + s.tipoSimbolo.name().toString()  + "</td></tr>";
        }

        reportTabla += "</TABLE>>]; }";

        FileWriter file = null;
        try {
            file = new FileWriter("simbolos.dot");
            file.write(reportTabla);
            file.close();
            Runtime.getRuntime().exec("dot -Tpdf simbolos.dot -o simbolos.pdf");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Object visitStart(GramaticaParser.StartContext ctx)
    {
        visitLrutinas(ctx.lrutinas());
        if(es3d) c3d.codigo.add("int main()\n{\n");
        visitLinstrucciones(ctx.linstrucciones());
        if(es3d) c3d.codigo.add("\nreturn 0;\n}");
        return true;
    }

    public Object visitLrutinas(GramaticaParser.LrutinasContext ctx){
        for(GramaticaParser.RutinasContext  rctx : ctx.rutinas())
            visitRutinas(rctx);
        return true;
    }

    public Object visitRutinas(GramaticaParser.RutinasContext ctx){
        if(ctx.subrutina() != null)
            visitSubrutina(ctx.subrutina());
        else if(ctx.funcion() != null)
            visitFuncion(ctx.funcion());
        return true;
    }

    public Object visitSubrutina(GramaticaParser.SubrutinaContext ctx){

        Entorno ent = pilaEnt.peek();

        if (ctx.id1.getText().equals(ctx.id2.getText()))
        {
            if(!ent.TablaSimbolo.containsKey((ctx.id1.getText() + TipoSimbolo.Subrutina.name()).toUpperCase()))
            {
                ArrayList<Simbolo> parametros = new ArrayList<Simbolo>();
                int pos = -1;

                for(GramaticaParser.ExprContext expCtx : ctx.lexpr().expr())
                    parametros.add(new Simbolo(expCtx.getText(), "", null, TipoSimbolo.Parametro, pos++));

                Subrutina subr = new Subrutina(ctx.id1.getText(), parametros, ctx.linstrucciones(), ctx.ldeclP());

                ent.nuevoSimbolo(ctx.id1.getText() + TipoSimbolo.Subrutina.name(),
                                new Simbolo(ctx.id1.getText(),
                                "Subrutina",
                                subr,
                                TipoSimbolo.Subrutina,
                                pilaEnt.peek().ultPosicion));
                                ent.ultPosicion++;
                return true;
            }else{
                errores.add(new ErrorAnalizador(ctx.id1.getLine(),
                        ctx.id1.getCharPositionInLine(),
                        "Ya existe una subrutina con ese nombre.",
                        ErrorAnalizador.ErrorTipo.Semantico));
            }
        }
        errores.add(new ErrorAnalizador(ctx.id1.getLine(),
                                        ctx.id1.getCharPositionInLine(),
                                        "Los identificadores de la subrutina no coinciden",
                                        ErrorAnalizador.ErrorTipo.Semantico));

        return true;
    }

    public Object visitFuncion(GramaticaParser.FuncionContext ctx){

        Entorno ent = pilaEnt.peek();

        if (ctx.id1.getText().equals(ctx.id2.getText()))
        {
            if(!ent.TablaSimbolo.containsKey((ctx.id1.getText() + TipoSimbolo.Subrutina.name()).toUpperCase()))
            {
                ArrayList<Simbolo> parametros = new ArrayList<Simbolo>();
                int pos = -1;

                for(GramaticaParser.ExprContext expCtx : ctx.lexpr().expr())
                    parametros.add(new Simbolo(expCtx.getText(), "", null, TipoSimbolo.Parametro, pos++));

                Funcion fnc = new Funcion(ctx.id1.getText(), parametros, ctx.linstrucciones(), ctx.ldeclP(), ctx.res.getText());

                ent.nuevoSimbolo(ctx.id1.getText() + TipoSimbolo.Funcion.name(),
                                new Simbolo(ctx.id1.getText(),
                                        "Funcion",
                                        fnc,
                                        TipoSimbolo.Funcion,
                                        pilaEnt.peek().ultPosicion));
                ent.ultPosicion++;
                return true;
            }else{
                errores.add(new ErrorAnalizador(ctx.id1.getLine(),
                        ctx.id1.getCharPositionInLine(),
                        "Ya existe una funcion con ese nombre.",
                        ErrorAnalizador.ErrorTipo.Semantico));
            }
        }
        errores.add(new ErrorAnalizador(ctx.id1.getLine(),
                ctx.id1.getCharPositionInLine(),
                "Los identificadores de la función no coinciden",
                ErrorAnalizador.ErrorTipo.Semantico));

        return true;
    }
    public Object visitLinstrucciones(GramaticaParser.LinstruccionesContext ctx)
    {
        for (GramaticaParser.InstruccionesContext ictx : ctx.instrucciones())
            visitInstrucciones(ictx);
        return true;
    }

    public Object visitInstrucciones(GramaticaParser.InstruccionesContext ctx)
    {
        if (ctx.declaracion() != null)
            visitDeclaracion(ctx.declaracion());
        else if(ctx.imprimir() != null)
            visitImprimir(ctx.imprimir());
        else if(ctx.asignacion() != null)
            visitAsignacion(ctx.asignacion());
        else if(ctx.arreglo_dec() != null)
            visitArreglo_dec(ctx.arreglo_dec());
        else if(ctx.arreglo_asig_all() != null)
            visitArreglo_asig_all(ctx.arreglo_asig_all());
        else if(ctx.arreglo_din_dec() != null)
            visitArreglo_din_dec(ctx.arreglo_din_dec());
        else if(ctx.arreglo_asig_tam() != null)
            visitArreglo_asig_tam(ctx.arreglo_asig_tam());
        else if(ctx.call() != null)
            visitCall(ctx.call());
        else if(ctx.cond_if() != null)
            visitCond_if(ctx.cond_if());
        else if(ctx.cond_if_else() != null)
            visitCond_if_else((ctx.cond_if_else()));
        else if(ctx.loop_do() != null)
            visitLoop_do(ctx.loop_do());
        else if(ctx.loop_while() != null)
            visitLoop_while(ctx.loop_while());
        else if(ctx.loop_exit() != null)
            visitLoop_exit(ctx.loop_exit());
        else if(ctx.loop_salto() != null)
            visitLoop_salto(ctx.loop_salto());
        else if(ctx.llamada() != null)
            visitLlamada(ctx.llamada());
        return true;
    }

    public Object visitDeclaracion(GramaticaParser.DeclaracionContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        for(Declaracion d : visitLid(ctx.lid())) {

            if (!this.es3d) {
                if(d != null){
                    String tipo = ctx.tipo().getText();

                    if (d.valor == null) {
                        Object v = null;

                        if (ctx.tipo().getText().toUpperCase().equals("INTEGER"))
                            v = 0;
                        else if (ctx.tipo().getText().toUpperCase().equals("REAL"))
                            v = 0.0;
                        else if (ctx.tipo().getText().toUpperCase().equals("LOGICAL"))
                            v = 'F';

                        Simbolo nuevo = new Simbolo(d.ID, tipo, v, TipoSimbolo.Variable, ent.ultPosicion);
                        ent.ultPosicion++;
                        ent.nuevoSimbolo(d.ID + TipoSimbolo.Variable.name(), nuevo);
                    } else {
                        if (!tipo.toUpperCase().equals(d.tipo)) {
                            errores.add(new ErrorAnalizador(
                                    d.linea,
                                    d.columna,
                                    "El valor no es del mismo tipo de la variable",
                                    ErrorAnalizador.ErrorTipo.Semantico));
                        }else {
                            Simbolo nuevo = new Simbolo(d.ID, tipo, d.valor, TipoSimbolo.Variable, ent.ultPosicion);
                            ent.ultPosicion++;
                            ent.nuevoSimbolo(d.ID + TipoSimbolo.Variable.name(), nuevo);

                        }

                    }
                    return true;
                }
            }
            else {
                if (ent.TablaSimbolo.containsKey((d.ID.toString() + TipoSimbolo.Variable.name()).toUpperCase())) {
                    Simbolo sim = ent.TablaSimbolo.get((d.ID.toString() + TipoSimbolo.Variable.name()).toUpperCase());

                    c3d.codigo.add(c3d.generarTemp() + " = " + ent.obtenerTamañoAnt() + " + " + sim.posicion + ";");
                    c3d.codigo.add("p = " + c3d.ultimoTemp() + ";");
                    Simbolo id = ent.Buscar(d.ID.toString() + TipoSimbolo.Variable.name());
                    c3d.codigo.add("stack[(int)p] = " + id.valor + ";");
                }
            }
        }
        return false;
    }

    public String visitTipo(GramaticaParser.TipoContext ctx)
    {
        return ctx.getText();
    }

    public ArrayList<Declaracion> visitLid(GramaticaParser.LidContext ctx){
        ArrayList<Declaracion> lst = new ArrayList<Declaracion>();
        for(GramaticaParser.AsigContext actx : ctx.asig() )
            lst.add((Declaracion)visit(actx));
        return lst;
    }

    public Declaracion visitAsigna(GramaticaParser.AsignaContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        if(!es3d) {
            if (!ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Variable.name()).toUpperCase())) {
                if ((Simbolo) visit(ctx.expr()) != null) {
                    Simbolo s = (Simbolo) visit(ctx.expr());
                    return new Declaracion(ctx.ID().getText(), s.valor, s.tipo, ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine());
                } else
                    return new Declaracion(ctx.ID().getText(), null, null, ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine());

            } else {
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "La variable ya existe en el entorno actual.",
                        ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }
        }
        else{
            return new Declaracion(ctx.ID().getText(), null, null, 0, 0);
        }

    }

    public Declaracion visitDecla(GramaticaParser.DeclaContext ctx){
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        {
            return new Declaracion(ctx.ID().getText(), null, null, ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine());
        }
        else{
            errores.add(new ErrorAnalizador(
                    ctx.ID().getSymbol().getLine(),
                    ctx.ID().getSymbol().getCharPositionInLine(),
                    "La variable ya existe en el entorno actual.",
                    ErrorAnalizador.ErrorTipo.Semantico));
            return null;
        }
    }

    public Object visitAsignacion(GramaticaParser.AsignacionContext ctx){
        Entorno ent = pilaEnt.peek();
        ArrayList<Object> valores = new ArrayList<Object>();
        Simbolo s1 = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Variable.name());
        //if(ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        if(s1 != null)
        {
            //Simbolo s = ent.TablaSimbolo.get((ctx.ID().getText() + TipoSimbolo.Variable.name()).toUpperCase());
            Simbolo s2 = (Simbolo)visit(ctx.expr());
            s1.valor =  s2.valor;
            return true;
        }
        else if(ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase()))
        {
            Simbolo s = ent.TablaSimbolo.get((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase());
            Arreglo arr = ent.BuscarArreglo(ctx.ID().getText());

            if(arr.dimension == 0 ){
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "No se ha asignado tamaño al arreglo " + ctx.ID().getText(),
                        ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }

            if(s.tipo.toUpperCase().equals("CHARACTER")){
                Simbolo s2 = (Simbolo)visit(ctx.expr());
                String valor = s2.valor.toString();

                for(int i=0; i<valor.length(); i++) {
                    char c = valor.charAt(i);
                    valores.add(c);
                }
                //Actualizacion de valores
                s.valor = valores;
                arr.valores = valores;
                return true;
            }
            return null;

        }
        else
        {
            errores.add(new ErrorAnalizador(
                    ctx.ID().getSymbol().getLine(),
                    ctx.ID().getSymbol().getCharPositionInLine(),
                    "La variable no existe en el entorno actual.",
                    ErrorAnalizador.ErrorTipo.Semantico));
            return null;
        }
    }
    public Object visitArreglo_dec(GramaticaParser.Arreglo_decContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        ArrayList<Object> a = new ArrayList<Object>();
        Arreglo arr = new Arreglo(ctx.ID().getText(), ctx.tipo().getText(), Integer.valueOf(ctx.ENTERO().getText()), a);
        Simbolo nuevo = new Simbolo(ctx.ID().getText(), ctx.tipo().getText(), arr.valores, TipoSimbolo.Arreglo, ent.ultPosicion);
        ent.ultPosicion ++;
        ent.nuevoSimbolo(ctx.ID().getText() + TipoSimbolo.Arreglo.name(), nuevo);
        ent.agregarArreglo(ctx.ID().getText(), arr);
        return true;
    }

    public Object visitArreglo_din_dec(GramaticaParser.Arreglo_din_decContext ctx){
        Entorno ent = pilaEnt.peek();
        ArrayList<Object> a = new ArrayList<Object>();
        Arreglo arr = new Arreglo(ctx.ID().getText(), ctx.tipo().getText(), 0, a);
        Simbolo nuevo = new Simbolo(ctx.ID().getText(), ctx.tipo().getText(), arr.valores, TipoSimbolo.Arreglo, ent.ultPosicion);
        ent.ultPosicion ++;
        ent.nuevoSimbolo(ctx.ID().getText() + TipoSimbolo.Arreglo.name(), nuevo);
        ent.agregarArreglo(ctx.ID().getText(), arr);
        return true;
    }

    public Object visitArreglo_asig_tam(GramaticaParser.Arreglo_asig_tamContext ctx){
        Entorno ent = pilaEnt.peek();
        if(ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase()))
        {
            Simbolo s = ent.TablaSimbolo.get((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase());
            Arreglo arr = ent.BuscarArreglo(ctx.ID().getText());
            int tam = Integer.valueOf(ctx.ENTERO().getText());
            if(tam > 0){
                arr.dimension = tam;
                return true;
            }else{
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "Tamaño de arreglo no permitido.",
                        ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }
        }
        return null;
    }

    public Object visitArreglo_asig_all(GramaticaParser.Arreglo_asig_allContext ctx) {

        Entorno ent = pilaEnt.peek();
        ArrayList<Object> valores = new ArrayList<Object>();

        if(ent.TablaSimbolo.containsKey((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase()))
        {
            Simbolo s = ent.TablaSimbolo.get((ctx.ID().getText() + TipoSimbolo.Arreglo.name()).toUpperCase());
            Arreglo arr = ent.BuscarArreglo(ctx.ID().getText());

            if(arr.dimension == 0){
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "No se ha asignado tamaño al arreglo " + ctx.ID().getText(),
                        ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }

            if(ctx.lexpr().expr().size() <= arr.dimension) {

                if(!s.tipo.toUpperCase().equals("CHARACTER")) {
                    //Valores
                    for (GramaticaParser.ExprContext expCtx : ctx.lexpr().expr()) {
                        Simbolo s1 = (Simbolo) visit(expCtx);
                        if (s1 != null) {
                            if (s.tipo.toUpperCase().equals(s1.tipo.toUpperCase()))
                                valores.add(s1.valor);
                            else {
                                errores.add(new ErrorAnalizador(
                                        ctx.ID().getSymbol().getLine(),
                                        ctx.ID().getSymbol().getCharPositionInLine(),
                                        "El tipo del elemento no es igual al del arreglo.",
                                        ErrorAnalizador.ErrorTipo.Semantico));
                            }
                        }
                    }
                }
                else
                {
                    for (GramaticaParser.ExprContext expCtx : ctx.lexpr().expr()) {
                        Simbolo s2 = (Simbolo) visit(expCtx);
                        if (s.tipo.toUpperCase().equals(s2.tipo.toUpperCase())){
                            String valor = s2.valor.toString();

                            for(int i=0; i<valor.length(); i++){
                                char c = valor.charAt(i);
                                valores.add(c);
                            }
                        }
                        break;
                    }
                }

                //Actualizacion de valores
                s.valor = valores;
                arr.valores = valores;

                return true;

            }else{
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "La cantidad de elementos es mayor a la declarada.",
                        ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }
        }
        else
        {
            errores.add(new ErrorAnalizador(
                    ctx.ID().getSymbol().getLine(),
                    ctx.ID().getSymbol().getCharPositionInLine(),
                    "La variable no existe en el entorno actual.",
                    ErrorAnalizador.ErrorTipo.Semantico));
            return null;
        }
    }

    public Object visitCall(GramaticaParser.CallContext ctx){
        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Busco el simbolo de la subrutina en el entorno actual
        Simbolo simbRutina = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Subrutina.name());
        //Si no lo encuentro, error
        if (simbRutina == null)
            errores.add(new ErrorAnalizador(ctx.ID().getSymbol().getLine(),
                    ctx.ID().getSymbol().getCharPositionInLine(),
                    "La subrutina " + ctx.ID().getText() + " no existe.",
                    ErrorAnalizador.ErrorTipo.Semantico));
        else{
            //Creamos el nuevo entorno para la subrutina
            Entorno entSubr = new Entorno(ent);
            //Se obiene el valor del simbolo de la subrutina
            Subrutina subr = (Subrutina) simbRutina.valor;
            //Se comparan la cantidad de parametros

            if (subr.lparametros.size() == ctx.lexpr().expr().size() && subr.lparametros.size() == subr.ldeclaracionParam.getChildCount())
            {
                //Para cada expresion
                for (int i = 0; i < ctx.lexpr().expr().size(); i++)
                {
                    Simbolo v = (Simbolo)visit(ctx.lexpr().expr().get(i));
                    if(v.valor != null)
                        subr.lparametros.get(i).valor = v.valor;
                    subr.lparametros.get(i).tipo = subr.ldeclaracionParam.declParams(i).tipo().getText().toUpperCase();
                    Simbolo nuevo = new Simbolo(subr.lparametros.get(i).identificador, subr.lparametros.get(i).tipo, subr.lparametros.get(i).valor, TipoSimbolo.Variable, entSubr.ultPosicion);
                    //entSubr.nuevoSimbolo(subr.lparametros.get(i).identificador + TipoSimbolo.Variable.name(), subr.lparametros.get(i));
                    entSubr.nuevoSimbolo(subr.lparametros.get(i).identificador + TipoSimbolo.Variable.name(), nuevo);
                }
                entSubr.ultPosicion = subr.lparametros.size();
                //Se agrega el entorno
                pilaEnt.push(entSubr);
                //Se ejecutan las instrucciones que se guardaron en el simbolo
                visitLinstrucciones((GramaticaParser.LinstruccionesContext)subr.linstrucciones);
                //Al finalizar, se quita el entorno.
                pilaEnt.pop();
            } else
                if(subr.lparametros.size() == 1 && subr.lparametros.get(0).identificador==""){
                    entSubr.ultPosicion = subr.lparametros.size();
                    //Se agrega el entorno
                    pilaEnt.push(entSubr);
                    //Se ejecutan las instrucciones que se guardaron en el simbolo
                    visitLinstrucciones((GramaticaParser.LinstruccionesContext)subr.linstrucciones);
                    //Al finalizar, se quita el entorno.
                    pilaEnt.pop();
                }else {
                    errores.add(new ErrorAnalizador(ctx.ID().getSymbol().getLine(),
                            ctx.ID().getSymbol().getCharPositionInLine(),
                            "La cantidad de parámetros no coincide.", ErrorAnalizador.ErrorTipo.Semantico));
                }
        }
        return true;
    }

    public Object visitLlamada(GramaticaParser.LlamadaContext ctx){
        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Busco el simbolo de la subrutina en el entorno actual
        Simbolo simFuncion = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Funcion.name());
        //Si no lo encuentro, error
        if (simFuncion == null) {
            errores.add(new ErrorAnalizador(ctx.ID().getSymbol().getLine(),
                    ctx.ID().getSymbol().getCharPositionInLine(),
                    "La función " + ctx.ID().getText() + " no existe.",
                    ErrorAnalizador.ErrorTipo.Semantico));
            return null;
        }
        else{
            //Creamos el nuevo entorno para la funcion
            Entorno entFun = new Entorno(ent);
            //Se obiene el valor del simbolo de la funcion
            Funcion fun = (Funcion) simFuncion.valor;

            //Se compara la cantidad de parametros
            if (fun.lparametros.size() == ctx.lexpr().expr().size() && fun.lparametros.size() == fun.ldeclaracionParam.getChildCount())
            {
                //Para cada parametro, obtenemos su valor y guardamos el simbolo en el entorno de la funcion.
                for (int i = 0; i < ctx.lexpr().expr().size(); i++)
                {
                    Simbolo v = (Simbolo)visit(ctx.lexpr().expr().get(i));
                    if(v.valor != null)
                        fun.lparametros.get(i).valor = v.valor;
                    fun.lparametros.get(i).tipo = fun.ldeclaracionParam.declParams(i).tipo().getText().toUpperCase();
                    Simbolo nuevo = new Simbolo(fun.lparametros.get(i).identificador, fun.lparametros.get(i).tipo, fun.lparametros.get(i).valor, TipoSimbolo.Variable, entFun.ultPosicion);
                    //entSubr.nuevoSimbolo(subr.lparametros.get(i).identificador + TipoSimbolo.Variable.name(), subr.lparametros.get(i));
                    entFun.nuevoSimbolo(fun.lparametros.get(i).identificador + TipoSimbolo.Variable.name(), nuevo);
                }
                entFun.ultPosicion = fun.lparametros.size();
                //Se agrega el entorno
                pilaEnt.push(entFun);
                //Se ejecutan las instrucciones que se guardaron en el simbolo
                visitLinstrucciones((GramaticaParser.LinstruccionesContext)fun.linstrucciones);

                //Obtener el valor que devuelve la funcion y guardarlo en el simbolo
                Simbolo res = entFun.Buscar(fun.resul.toString() + TipoSimbolo.Variable.name());
                Funcion f = (Funcion)simFuncion.valor;
                f.valor = res.valor;
                simFuncion.valor = f;

                //Al finalizar, se quita el entorno.
                pilaEnt.pop();
            } else
            if(fun.lparametros.size() == 1 && fun.lparametros.get(0).identificador==""){
                entFun.ultPosicion = fun.lparametros.size();
                //Se agrega el entorno
                pilaEnt.push(entFun);
                //Se ejecutan las instrucciones que se guardaron en el simbolo
                visitLinstrucciones((GramaticaParser.LinstruccionesContext)fun.linstrucciones);

                //Obtener el valor que devuelve la funcion y guardarlo en el simbolo
                Simbolo res = entFun.Buscar(fun.resul.toString() + TipoSimbolo.Variable.name());
                simFuncion.valor = res.valor;

                //Al finalizar, se quita el entorno.
                pilaEnt.pop();
            }else {
                errores.add(new ErrorAnalizador(ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "La cantidad de parámetros no coincide.", ErrorAnalizador.ErrorTipo.Semantico));
                return null;
            }
        }
        return simFuncion;
    }
    public Object visitImprimir(GramaticaParser.ImprimirContext ctx)
    {
        for (GramaticaParser.ExprContext expCtx : ctx.lexpr().expr()) {
            Simbolo s = (Simbolo) visit(expCtx);
            if(!es3d) {
                if (s != null) {
                    if (s.tipoSimbolo.name().equals("Variable")) {
                        setPrint(s.valor.toString() + '\n');
                        System.out.println(s.valor.toString());
                    } else if (s.tipoSimbolo.name().equals("Funcion")) {
                        Funcion f = (Funcion) s.valor;
                        setPrint(f.valor.toString() + '\n');
                        System.out.println(f.valor.toString());
                    } else if (s.tipoSimbolo.name().equals("Arreglo")) {
                        if (!s.tipo.toUpperCase().equals("CHARACTER")) {
                            setPrint(s.valor.toString() + '\n');
                            System.out.println(s.valor);
                        } else {
                            String cadena = "";
                            ArrayList<Object> arr = new ArrayList<Object>();
                            arr.addAll((ArrayList) s.valor);
                            for (Object c : arr) {
                                cadena += c.toString();
                            }
                            setPrint(cadena + '\n');
                            System.out.println(cadena);
                        }
                    }
                }
            }
            else
            {
                Entorno ent = pilaEnt.peek();
                Simbolo sim = null;
                if(s.tipo.toUpperCase().equals("IDSTR") || s.tipo.toUpperCase().equals("IDFLOAT"))
                    sim = ent.Buscar(s.identificador + TipoSimbolo.Variable.name());

                switch(s.tipo)
                {
                    case "FLOAT":
                        c3d.codigo.add("printf(\"%f\\n\", " + s.valor + ");");
                        break;

                    case "STR":
                        c3d.codigo.add("p = " + s.valor + ";");
                        c3d.codigo.add("imprimir_string();");
                        break;

                    case "IDSTR":
                        if(sim != null){
                            c3d.codigo.add(c3d.generarTemp() + " = " + ent.obtenerTamañoAnt() + " + " + sim.posicion + ";");
                            c3d.codigo.add("P = " + c3d.ultimoTemp() + ";");
                            c3d.codigo.add("imprimir_variable();");
                        }

                    case "IDFLOAT":
                        if(sim != null){
                            c3d.codigo.add(c3d.generarTemp() + " = " + ent.obtenerTamañoAnt() + " + " + sim.posicion + ";");
                            c3d.codigo.add("p = " + c3d.ultimoTemp() + ";");
                            c3d.codigo.add("imprimir_var_Entero();");
                        }
                }

            }
        }

        return true;
    }

    public Object visitLexpr(GramaticaParser.LexprContext ctx)
    {
        for(GramaticaParser.ExprContext ectx : ctx.expr())
            visit(ectx);
        return true;
    }

    public Object visitCond_if(GramaticaParser.Cond_ifContext ctx){

        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Creamos el nuevo entorno para el if
        Entorno entIf = new Entorno(ent);
        Simbolo s = (Simbolo)visit(ctx.expr());
        if(s.valor.toString().equals("T")){
            System.out.println("si entra en el if");
            //Se agrega el entorno
            pilaEnt.push(entIf);
            //Se ejecutan las instrucciones
            visitLinstrucciones(ctx.linstrucciones());
            //Al finalizar, se quita el entorno.
            pilaEnt.pop();

            //Para revisar si viene EXIT o CYCLE
            for(int i = 0; i < ctx.linstrucciones().instrucciones().size(); i++){
                if(ctx.linstrucciones().instrucciones().get(i).loop_exit() != null){
                    return true;
                }
                else if(ctx.linstrucciones().instrucciones().get(i).loop_salto() != null) {
                    return false;
                }
            }

        }
        return null;
    }

    public Object visitCond_if_else(GramaticaParser.Cond_if_elseContext ctx){

        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Creamos el nuevo entorno para el if
        Entorno entIf = new Entorno(ent);
        Simbolo s = (Simbolo)visit(ctx.expr());
        if(s.valor.toString().equals("T")){
            System.out.println("si entra en el if");
            //Se agrega el entorno
            pilaEnt.push(entIf);
            //Se ejecutan las instrucciones
            visitLinstrucciones(ctx.li1);
            //Al finalizar, se quita el entorno.
            pilaEnt.pop();
            return true;
        }
        else{
            System.out.println("si entra en el else");
            //Se agrega el entorno
            pilaEnt.push(entIf);
            //Se ejecutan las instrucciones
            visitLinstrucciones(ctx.li2);
            //Al finalizar, se quita el entorno.
            pilaEnt.pop();
            return true;
        }
    }

    public Object visitLoop_do(GramaticaParser.Loop_doContext ctx){

        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Creamos el nuevo entorno para el if
        Entorno entDo = new Entorno(ent);

        visitAsignacion(ctx.asignacion());

        //Se agrega el entorno
        pilaEnt.push(entDo);

        Simbolo s = ent.TablaSimbolo.get((ctx.asignacion().ID().getText() + TipoSimbolo.Variable.name()).toUpperCase());
        if(s != null){
            int inicio = Integer.valueOf(s.valor.toString());

            Simbolo s1 = (Simbolo)visit(ctx.fin);
            int fin = Integer.valueOf(s1.valor.toString());

            Simbolo s2 = (Simbolo)visit(ctx.salto);
            int salto = Integer.valueOf(s2.valor.toString());

            out:
            for(int i = inicio; i<=fin; i+=salto ){
                String ins="";

                for(int j = 0; j < ctx.linstrucciones().instrucciones().size(); j++) {
                    //ins = ctx.linstrucciones().instrucciones().get(j).getText().toString().replaceAll("\n", "");
                    if(ctx.linstrucciones().instrucciones().get(j).loop_exit() != null){
                        break out;
                    }
                    else if(ctx.linstrucciones().instrucciones().get(j).cond_if() != null){
                        Object res = visitCond_if(ctx.linstrucciones().instrucciones().get(j).cond_if());
                        if(res != null){
                            if ((Boolean) res == true) break out;
                            else if ((Boolean)res == false){
                                inicio = inicio + salto;
                                s.valor = inicio;
                                continue  out;
                            }
                        }

                    }
                    else
                        visitInstrucciones(ctx.linstrucciones().instrucciones().get(j));
                }
                //visitLinstrucciones(ctx.linstrucciones());
                inicio = inicio + salto;
                s.valor = inicio;
            }
        }

        //Al finalizar, se quita el entorno.
        pilaEnt.pop();

        return true;
    }

    public Object visitLoop_while(GramaticaParser.Loop_whileContext ctx)
    {
        //Entorno actual
        Entorno ent = pilaEnt.peek();
        //Creamos el nuevo entorno para el if
        Entorno entWh = new Entorno(ent);

        //String id = ctx.expr().children.get(0).getText().toString();
        Simbolo s = (Simbolo)visit(ctx.expr());

        if(s != null) {
            pilaEnt.push(entWh);
            out:
            while (s.valor.toString().equals("T")) {
                System.out.println("si entra en el do while");
                //Se ejecutan las instrucciones

                for(int j = 0; j < ctx.linstrucciones().instrucciones().size(); j++) {
                    //ins = ctx.linstrucciones().instrucciones().get(j).getText().toString().replaceAll("\n", "");
                    if(ctx.linstrucciones().instrucciones().get(j).loop_exit() != null){
                        break out;
                    }
                    else if(ctx.linstrucciones().instrucciones().get(j).cond_if() != null){
                        Object res = visitCond_if(ctx.linstrucciones().instrucciones().get(j).cond_if());
                        //if ((Integer)res == 1) break out;
                        if(res != null){
                            if ((Boolean) res == true) break out;
                        }
                    }
                    else {
                        visitInstrucciones(ctx.linstrucciones().instrucciones().get(j));
                        s = (Simbolo) visit(ctx.expr());
                    }
                }
                //visitLinstrucciones(ctx.linstrucciones());
            }
        }
        //Al finalizar, se quita el entorno.
        pilaEnt.pop();
        return true;
    }

    public Object visitLoop_exit(GramaticaParser.Loop_exitContext ctx)
    {
        return true;
    }

    public Object visitLoop_salto(GramaticaParser.Loop_saltoContext ctx)
    {
        return true;
    }
    public Object visitNotExpr(GramaticaParser.NotExprContext ctx)
    {
        Simbolo der = (Simbolo)visit(ctx.der);
        String op = ctx.op.getText();

        if(der.tipo.toString().toUpperCase().equals("LOGICAL"))
        {
            Character val = ((der.valor.toString().equals("F") ) ? 'T' : 'F');
            return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
        }
        else {
            errores.add(new ErrorAnalizador(
                    ctx.op.getLine(),
                    ctx.op.getCharPositionInLine(),
                    "El tipo de dato no es correcto.",
                    ErrorAnalizador.ErrorTipo.Semantico
            ));
            return null;
        }
    }

    public Object visitOpExpr(GramaticaParser.OpExprContext ctx){
        Simbolo izq = (Simbolo)visit(ctx.izq);
        Simbolo der = (Simbolo)visit(ctx.der);
        String op = ctx.op.getText();

        if(es3d){
            if(op.toUpperCase().equals(".AND."))
                return c3d.obtenerAnd(izq.valor.toString(), der.valor.toString());

            Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), "FLOAT");
            c3d.codigo.add(sim3d.valor + " = " + izq.valor + op + der.valor + ";");
            return sim3d;

        }

        switch (op) {
            case "**":
                if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "INTEGER", (int)Math.pow((int)izq.valor, (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((int)Math.pow((int)izq.valor, (float)der.valor)), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "REAL", (float)(Math.pow((float)izq.valor, (int)der.valor)), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)(Math.pow((float)izq.valor, (float)der.valor)), TipoSimbolo.Variable, -1 );
                else
                    return new Simbolo("", "INTEGER", (int)Math.pow((int)izq.valor, (int)der.valor), TipoSimbolo.Variable, -1 );

            case "*":
                if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "INTEGER", (int)izq.valor * (int)der.valor, TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((int)izq.valor * (float)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor * (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor * (float)der.valor), TipoSimbolo.Variable, -1 );
                else
                    return new Simbolo("", "INTEGER", (int)izq.valor * (int)der.valor, TipoSimbolo.Variable, -1 );

            case "/":
                if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "INTEGER", (int)((int)izq.valor / (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((int)izq.valor / (float)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor / (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor / (float)der.valor), TipoSimbolo.Variable, -1 );
                else
                    return new Simbolo("", "INTEGER", (int)izq.valor / (int)der.valor, TipoSimbolo.Variable, -1 );
            case "+":
                if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "INTEGER", (int)((int)izq.valor + (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((int)izq.valor + (float)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor + (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor + (float)der.valor), TipoSimbolo.Variable, -1 );
                else
                    return new Simbolo("", "INTEGER", (int)izq.valor + (int)der.valor, TipoSimbolo.Variable, -1 );
            case "-":
                if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "INTEGER", (int)((int)izq.valor - (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("INTEGER") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((int)izq.valor - (float)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("INTEGER"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor - (int)der.valor), TipoSimbolo.Variable, -1 );
                else if(izq.tipo.toUpperCase().equals("REAL") && der.tipo.toUpperCase().equals("REAL"))
                    return new Simbolo("", "REAL", (float)((float)izq.valor - (float)der.valor), TipoSimbolo.Variable, -1 );
                else
                    return new Simbolo("", "INTEGER", (int)izq.valor - (int)der.valor, TipoSimbolo.Variable, -1 );
            case "==":
                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor == (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor == (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("STRING"))
                    {
                        Character val = (izq.valor.toString().equals(der.valor.toString()) ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case "/=":
                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor != (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor != (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(!izq.valor.getClass().getSimpleName().toUpperCase().equals("STRING"))
                    {
                        Character val = (izq.valor.toString().equals(der.valor.toString()) ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case ">":
                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor > (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor > (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case "<":

                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor < (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor < (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case ">=":
                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor >= (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor >= (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case "<=":
                //Tipos tienen que ser iguales
                if(!izq.valor.getClass().getSimpleName().equals(der.valor.getClass().getSimpleName())) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.valor.getClass().getSimpleName().toUpperCase().equals("INTEGER"))
                    {
                        Character val = ((int)izq.valor <= (int)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else if(izq.valor.getClass().getSimpleName().toUpperCase().equals("DOUBLE"))
                    {
                        Character val = ((double)izq.valor <= (double)der.valor ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }

                    else return null;
                }

            case ".and.":

                //Tipos tienen que ser iguales
                if(!izq.tipo.toString().equals(der.tipo.toString()) ) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.tipo.toString().toUpperCase().equals("LOGICAL"))
                    {
                        Character val = ((izq.valor.toString().equals("T") && der.valor.toString().equals("T")) ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }

            case ".or.":

                //Tipos tienen que ser iguales
                if(!izq.tipo.toString().equals(der.tipo.toString()) ) {
                    errores.add(new ErrorAnalizador(
                            ctx.op.getLine(),
                            ctx.op.getCharPositionInLine(),
                            "Los tipos de datos no son iguales.",
                            ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
                }
                else
                {
                    if(izq.tipo.toString().toUpperCase().equals("LOGICAL"))
                    {
                        Character val = ((izq.valor.toString().equals("T") || der.valor.toString().equals("T")) ? 'T' : 'F');
                        return new Simbolo("", "LOGICAL", val, TipoSimbolo.Variable, -1 );
                    }
                    else return null;
                }


            default :
                errores.add(new ErrorAnalizador(
                    ctx.op.getLine(),
                    ctx.op.getCharPositionInLine(),
                    "Operacion no valida",
                    ErrorAnalizador.ErrorTipo.Semantico
                    ));
                    return null;
        }
    }

    public Simbolo visitLlamExpr(GramaticaParser.LlamExprContext ctx){
        return (Simbolo) visit(ctx.llamada());
    }
    public Simbolo visitParenExpr(GramaticaParser.ParenExprContext ctx){
        return (Simbolo) visit(ctx.expr());
    }

    public Simbolo visitIntExpr(GramaticaParser.IntExprContext ctx){

        if(!es3d)
            return new Simbolo("","INTEGER", Integer.valueOf(ctx.getText()), TipoSimbolo.Variable, -1);
        Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), "FLOAT");
        c3d.codigo.add(sim3d.valor + " = " + ctx.getText() + ";");
        return sim3d;
    }

    public Simbolo visitStrExpr(GramaticaParser.StrExprContext ctx)
    {
        if(!es3d){
            return new Simbolo("","CHARACTER", ctx.getText().toString().replaceAll("\"",""), TipoSimbolo.Variable, -1);
        }
        Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), "STR");
        c3d.codigo.add(sim3d.valor + " = h;");
        for(char i : String.valueOf(ctx.getText()).toCharArray())
        {
            c3d.codigo.add("heap[(int)h] = " + (int)i + ";");
            c3d.codigo.add("h = h + 1;");
        }
        c3d.codigo.add("heap[(int)h] = -1;");
        c3d.codigo.add("h = h + 1;");
        return sim3d;

    }

    public Simbolo visitDecExpr(GramaticaParser.DecExprContext ctx){

        if(!es3d)
            return new Simbolo("","REAL", Float.valueOf(ctx.getText()), TipoSimbolo.Variable, -1);
        Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), "FLOAT");
        c3d.codigo.add(sim3d.valor + " = " + ctx.getText() + ";");
        return sim3d;
    }

    public Simbolo visitAccArr(GramaticaParser.AccArrContext ctx){
        Entorno ent = pilaEnt.peek();
        if(ent.Buscar(ctx.ID().getText() + TipoSimbolo.Arreglo.name())!=null) {
            Simbolo s = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Arreglo.name());
            Arreglo arr = ent.BuscarArreglo(ctx.ID().getText());
            Simbolo s1 = (Simbolo)visit(ctx.expr());
            int posicion = Integer.valueOf(s1.valor.toString());
            String tipo = s.tipo.toString().toUpperCase();
            if(posicion > 0 && posicion <= arr.valores.size()){
                Object valor = arr.valores.get(posicion-1);
                //Object valor = (ArrayList)s.valor.get(posicion-1);
                return new Simbolo("", tipo, valor, TipoSimbolo.Variable, -1);
            }else{
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "Error al acceder al arreglo. Indice incorrecto.",
                        ErrorAnalizador.ErrorTipo.Semantico
                ));
            }

        }
        return null;
    }


    public Simbolo visitIdExpr(GramaticaParser.IdExprContext ctx) {

        Entorno ent = pilaEnt.peek();

        if(!es3d) {
            if (ent.Buscar(ctx.ID().getText() + TipoSimbolo.Variable.name()) != null) {
                Simbolo id = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Variable.name());
                return id;
            } else if (ent.Buscar(ctx.ID().getText() + TipoSimbolo.Arreglo.name()) != null) {
                Simbolo id = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Arreglo.name());
                return id;
            } else {
                errores.add(new ErrorAnalizador(
                        ctx.ID().getSymbol().getLine(),
                        ctx.ID().getSymbol().getCharPositionInLine(),
                        "La variable " + ctx.ID().getText() + " no existe.",
                        ErrorAnalizador.ErrorTipo.Semantico
                ));
                return null;
            }
        }
        Simbolo id = ent.Buscar(ctx.ID().getText() + TipoSimbolo.Variable.name());
        Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), id.tipo.toUpperCase().equals("CHARACTER") ? "IDSTR" : "IDFLOAT");
        sim3d.identificador = ctx.ID().getText();

        c3d.codigo.add(c3d.generarTemp() + " = " + ent.obtenerTamañoAnt() + " + " + id.posicion + ";");
        c3d.codigo.add("p = " + c3d.ultimoTemp() + ";");
        c3d.codigo.add(sim3d.valor + " = stack[(int)p];");
        return sim3d;

    }

    public Simbolo visitBoolExpr(GramaticaParser.BoolExprContext ctx)
    {
        if(!es3d) {
            if (ctx.getText().toUpperCase().equals(".TRUE."))
                return new Simbolo("", "LOGICAL", "T", TipoSimbolo.Variable, -1);
            else
                return new Simbolo("", "LOGICAL", "F", TipoSimbolo.Variable, -1);
        }
        Simbolo sim3d = new Simbolo(TipoSimbolo.C3D, c3d.generarTemp(), "FLOAT");
        c3d.codigo.add(sim3d.valor + " = " + (ctx.getText().toUpperCase().equals(".TRUE.") ? "1" : "0") + ";");
        return sim3d;
    }

}
