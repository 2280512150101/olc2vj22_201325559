package Entorno;


import Tipos.Arreglo;
import com.sun.source.tree.NewArrayTree;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

public class Entorno {

    public HashMap<String, Simbolo> TablaSimbolo;
    public HashMap<String, Arreglo> TablaArreglos;
    public Entorno padre;
    public Entorno siguiente;
    public int ultPosicion;

    public Entorno(Entorno padre) {
        this.padre = padre;
        TablaSimbolo = new HashMap<String, Simbolo>();
        TablaArreglos = new HashMap<String, Arreglo>();
        this.ultPosicion = 0;
    }


    public void nuevoSimbolo(String nombre, Simbolo nuevo)
    {
        if (TablaSimbolo.containsKey(nombre.toUpperCase())){
            //agregar a la lista de errores
            System.out.println("La variable " + nombre + " ya existe");
        }
        else{
            TablaSimbolo.put(nombre.toUpperCase(), nuevo);
        }
    }

    public Simbolo Buscar(String nombre){
        for(Entorno ent = this; ent != null; ent = ent.padre){
            if(ent.TablaSimbolo.containsKey(nombre.toUpperCase())){
                return ent.TablaSimbolo.get(nombre.toUpperCase());
            }
        }

        return null;
    }


    //ARREGLOS

    public void agregarArreglo(String nombre, Arreglo nuevo)
    {
        if (TablaArreglos.containsKey(nombre.toUpperCase())){
        }
        else{
            TablaArreglos.put(nombre.toUpperCase(), nuevo);
        }
    }

    public Arreglo BuscarArreglo(String nombre){

        for(Entorno ent = this; ent != null; ent = ent.padre){
            if(ent.TablaArreglos.containsKey(nombre.toUpperCase())){
                return ent.TablaArreglos.get(nombre.toUpperCase());
            }
        }
        return null;
    }

    public HashMap<String, Simbolo> getTablaSimbolo() {
        return TablaSimbolo;
    }

    public int obtenerTamañoAnt(){
        int size = 0;
        for(Entorno ent = this.padre; ent != null; ent = ent.padre)
            size += ent.TablaSimbolo.size();

        return size;
    }
}
