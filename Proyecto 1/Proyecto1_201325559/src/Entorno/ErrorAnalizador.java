package Entorno;

public class ErrorAnalizador {

    public int fila;
    public int columna;
    public String descripcion;
    public ErrorTipo tipo;

    public enum ErrorTipo{
        Lexico,
        Sintactico,
        Semantico
    }

    public ErrorAnalizador(int fila, int columna, String descripcion, ErrorTipo tipo){
        this.fila = fila;
        this.columna = columna;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }

    @Override
    public String toString(){
        return "<tr><td>" + (fila < -100 ? "Fila" : fila) + "</td>" +
                "<td>" + (columna < -100 ? "Columna" : columna) + "</td>" +
                "<td>" + descripcion + "</td>" +
                "<td>" + (tipo == null ? "Tipo de error" : tipo) + "</td></tr>";
    }
}
