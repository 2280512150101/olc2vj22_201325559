grammar Gramatica;

options {caseInsensitive = true; }

PROGRAM_R   : 'program';
IMPLICIT_R  : 'implicit';
NONE_R      : 'none';
END_R       : 'end';

INTEGER_R   : 'integer';
REAL_R      : 'real';
COMPLEX_R   : 'complex';
CHARACTER_R : 'character';
LOGICAL_R   : 'logical';

PRINT_R     : 'print';

TRUE_R      : '.true.';
FALSE_R     : '.false.';

AND_R       : '.and.';
OR_R        : '.or.';
NOT_R       : '.not.';

INTENT      : 'intent';
IN          : 'in';

DIMENSION_R : 'dimension';
ALLOCATABLE_R : 'allocatable';
ALLOCATE_R  : 'allocate';

SUBROUTINE_R : 'subroutine';
CALL_R       : 'call';

FUNCION_R    : 'function';
RESULT_R     : 'result';

IF_R        : 'if';
THEN_R      : 'then';
ELSE_R      : 'else';

DO_R        : 'do';
WHILE_R     : 'while';

EXIT_R      : 'exit';
CYCLE_R     : 'cycle';

ENTERO      : [0-9]* ;
DECIMAL     : [0-9]* '.' [0-9]* ;
ID          : [_a-zA-Z][a-zA-Z0-9_]* ;
CADENA      : '"' (~["\r\n]+ | '""')* '"' ;
WS          : [' '\t\r\n]+ -> skip ;


start           : lrutinas PROGRAM_R ID  '\n' IMPLICIT_R NONE_R '\n' linstrucciones END_R PROGRAM_R ID EOF
                ;

lrutinas        : (rutinas)*
                ;

rutinas         : subrutina
                | funcion
                ;

funcion         : FUNCION_R id1=ID '(' lexpr ')' RESULT_R '(' res=ID ')' '\n' IMPLICIT_R NONE_R '\n' ldeclP linstrucciones END_R FUNCION_R id2=ID '\n'
                ;

subrutina       : SUBROUTINE_R  id1=ID '(' lexpr ')' '\n' IMPLICIT_R NONE_R '\n' ldeclP linstrucciones END_R SUBROUTINE_R id2=ID '\n'
                ;

ldeclP          : declParams+
                ;

declParams      : tipo ',' INTENT '(' IN ')' ':' ':' ID '\n'
                ;

linstrucciones  : instrucciones (instrucciones) *
                ;

instrucciones   : declaracion
                | asignacion
                | arreglo_dec
                | arreglo_din_dec
                | arreglo_asig_all
                | arreglo_asig_tam
                | call
                | imprimir
                | cond_if
                | cond_if_else
                | loop_do
                | loop_while
                | comment
                | loop_salto
                | loop_exit
                | llamada
                ;

declaracion     : tipo ':' ':' lid '\n'
                ;

tipo            : INTEGER_R
                | REAL_R
                | COMPLEX_R
                | LOGICAL_R
                | CHARACTER_R
                ;

lid             : asig (',' asig)*
                ;

asig            : ID '=' expr       #asigna
                | ID                #decla
                ;

asignacion      : ID '=' expr '\n'
                ;

arreglo_dec     : tipo ',' DIMENSION_R '(' ENTERO ')' ':'':' ID '\n'
                ;

arreglo_din_dec : tipo ','  ALLOCATABLE_R ':' ':' ID '(' ':' ')' '\n'
                ;

arreglo_asig_all : ID '=' '(' '/' lexpr '/' ')' '\n'
                 ;

arreglo_asig_tam : ALLOCATE_R '(' ID '(' ENTERO ')' ')' '\n'
                 ;

call            : CALL_R ID '(' lexpr ')' '\n'
                ;

llamada         : ID '(' lexpr ')' '\n'
                ;

imprimir        : PRINT_R '*' ',' lexpr '\n'
                ;

lexpr           : expr (',' expr)*
                ;

cond_if_else    : IF_R '(' expr ')' THEN_R '\n' li1=linstrucciones ELSE_R '\n' li2=linstrucciones END_R IF_R  '\n'
                ;

cond_if         : IF_R '(' expr ')' THEN_R '\n' linstrucciones END_R IF_R '\n'
                ;

loop_do         : DO_R asignacion ',' fin=expr ',' salto=expr '\n' linstrucciones END_R DO_R '\n'
                ;

loop_while      : DO_R WHILE_R '(' expr ')' '\n' linstrucciones END_R DO_R '\n'
                ;

loop_exit       : EXIT_R '\n'
                ;

loop_salto      : CYCLE_R '\n'
                ;

comment         : '!' ~( '\r' | '\n' )* '\n'
                ;

expr            : izq=expr op='**' der=expr                 #opExpr
                | izq=expr op=('*'|'/') der=expr            #opExpr
                | izq=expr op=('+'|'-') der=expr            #opExpr
                | izq=expr op=('=='|'/=') der=expr          #opExpr
                | izq=expr op=('<'|'>'|'>='|'<=') der=expr  #opExpr
                | op=NOT_R der=expr                         #notExpr
                | izq=expr op=(AND_R | OR_R ) der=expr      #opExpr
                | '(' expr ')'                              #parenExpr
                | ent=ENTERO                                #intExpr
                | id=ID                                     #idExpr
                | dec=DECIMAL                               #decExpr
                | TRUE_R                                    #boolExpr
                | FALSE_R                                   #boolExpr
                | CADENA                                    #strExpr
                | ID '[' expr ']'                           #accArr
                | llamada                                   #llamExpr
                ;